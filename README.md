# localcertman

A developer-friendly helper for generating SSL certs signed by the same CA.

# Security

**DO NOT USE IN PRODUCTION**

The purpose of `localcertman` is to allow a single local developer to issue SSL certs that are 'valid' on their developer machine.

To accomplish this, `localcertman` runs a semi-persistent [step-ca](https://smallstep.com/certificates/) as the Certificate Authority
and provides a simplified iterface to [step-cli](https://smallstep.com/cli/) for issuing certificates.

# Setup

`localcertman` requires:
* [docker](https://docs.docker.com/get-docker/)
* [docker-compose](https://docs.docker.com/compose/install/)

# Configuration

If you want configuration, just use `step-ca` and `step-cli` directly.

`localcertman` is about simplicity:
* I want a cert for `X`
* I don't want certificate errors for `X`

# Usage

```bash
# Issue a cert for FQDN
./issue FQDN
```
or: 
```bash
# Issue a cert for FQDN
# + Add an /etc/hosts entry for FQDN
./issue FQDN 127.0.0.1
```
...yep. It's that easy!

Example:

```bash
./issue www.localhost 127.0.0.1
# www.localhost.crt and www.localhost.key are written to ./certs/
```

# Trusting the CA

The first time we generate certs with `localcertman`, we need to install the generated `./certs/localcertman_bundle.crt` as a trusted CA.

> **WARNING** This has security implications: if your `step-ca` private keys (inside the `step-ca` container) get compromised,
> an attacker could potentially sign bogus certs for well-known sites (e.g. google, facebook, etc.) and perform an SSL MITM.

If you don't install the root CA, everything will still work, but you may get certificate errors that you will have to ignore.

Alternatively, many programs (e.g `curl`) allow you to specify a trusted CA cert that will only be trusted for by that program (rather than system-wide)

```bash
# Trust the new root CA and install the certs into firefox
./trush.ca.sh
```

Most applications will respect system-wide trusted certs, but some (like firefox) may need additional configuration.
If you do need to manually install the certs: `./certs/localcertman_root.crt` is the root certificate and `./certs/localcertman_bundle.crt` is the root cert + intermediate cert.
# Testing Your Certs

If you have `openssl` installed, you can use `./test-server.sh` to test out your certs.

```bash
# Generate a cert like:
#./issue www.localhost 127.0.0.1

# Start the test server
./test-server.sh www.localhost

# Wait for a connection to :4433
# Then, type HELLO<CTRL+D>
```
It works like `netcat`, so once you get a connection, you can type a message and press `CTRL+D` to send it.


To connect:

```bash
curl https://www.localhost
```
or
```bash
firefox https://www.localhost
```

Tada! No cert errors!


# Notes

If you make manual changes to `localcertman` (e.g. to change defaults you don't like), use `./rebuild`.

> **WARNING** Using `rebuild` or deleting the persistent docker volume will cause a new `root_ca.crt` to be generated
> This means you will lose the original root CA and a new certificate / key will be generated.
> You will have to `./trust-ca.sh` again before new certificates are trusted.
