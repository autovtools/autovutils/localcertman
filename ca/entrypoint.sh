#!/bin/sh
set -e

function randpass(){
    LEN=${1:-64}
    base64 </dev/urandom | head -c "${LEN}"
}
SECRETS="${SECRETS:-${HOME}/secrets}"
mkdir -p "${SECRETS}"

CA_NAME="${CA_NAME:-LocalCA}"
PROVISIONER="${PROVISIONER:-dev@localhost}"

CA_FQDN="${CA_FQDN:-step-ca}"
CA_ADDR="${CA_ADDR:-0.0.0.0:9000}"

MAX_DURATION="${MAX_DURATION:-8760h}"
DEFAULT_DURATION="${DEFAULT_DURATION:-${MAX_DURATION}}"

ROOT_PASS_FILE="${PROVISIONER_PASS_FILE:-${SECRETS}/root.pass}"
PROVISIONER_PASS_FILE="${PROVISIONER_PASS_FILE:-${SECRETS}/provisioner.pass}"

CA_JSON="${HOME}/config/ca.json"
CA_CERT="${HOME}/certs/root_ca.crt"
INTERMEDIATE_CA_CERT="${HOME}/certs/intermediate_ca.crt"
CA_BUNDLE="${HOME}/certs/localcertman_bundle.crt"
CA_ROOT="${HOME}/certs/localcertman_root.crt"
CA_FINGERPRINT="${HOME}/certs/root_ca.fingerprint"

if [ ! -f "${CA_JSON}" ]; then

    randpass > "${ROOT_PASS_FILE}"
    randpass > "${PROVISIONER_PASS_FILE}"

    step ca init \
        --name "${CA_NAME}" \
        --provisioner "${PROVISIONER}" \
        --dns "${CA_FQDN}" \
        --address "${CA_ADDR}" \
        --password-file "${ROOT_PASS_FILE}" \
        --provisioner-password-file "${PROVISIONER_PASS_FILE}"

    # Increase certificate validity period
    #   https://github.com/smallstep/cli/issues/125
    #   https://github.com/smallstep/certificates/blob/master/docs/GETTING_STARTED.md#whats-inside-cajson
    JQ='.authority.provisioners[[.authority.provisioners[] | .name=="'${PROVISIONER}'"] | index(true)].claims |= (. + {"maxTLSCertDuration":"'${MAX_DURATION}'","defaultTLSCertDuration":"'${DEFAULT_DURATION}'"})'

    echo $(cat "${CA_JSON}" | jq "${JQ}" ) > "${CA_JSON}"

fi
step certificate fingerprint "${CA_CERT}" > "${CA_FINGERPRINT}"
step certificate bundle "${INTERMEDIATE_CA_CERT}" "${CA_CERT}" "${CA_BUNDLE}"
cp "${CA_CERT}" "${CA_ROOT}"

exec "$(which step-ca)" -password-file "${ROOT_PASS_FILE}" "${CA_JSON}"
