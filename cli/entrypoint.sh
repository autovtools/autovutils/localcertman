#!/bin/sh
ISSUE_FQDN="${1}"
SECRETS="${SECRETS:-${HOME}/secrets}"
export CERTS_OUT="${CERTS_OUT:-${HOME}/certs}"
export CA_URL="${CA_URL:-https://step-ca:9000}"
export ISSUER="${ISSUER:-dev@localhost}"
export PROVISIONER_PASSWORD_FILE=${PROVISIONER_PASSWORD_FILE:-${SECRETS}/provisioner.pass}
# The root ca cert
export CA_CERT="${CA_CERT:-${HOME}/certs/root_ca.crt}"
# Fingerprint for the root ca cert
export CA_FINGERPRINT="${CA_FINGERPRINT:-${HOME}/certs/root_ca.fingerprint}"
# A bundle of root ca cert + intermediate(s)
export LOCAL_CA_CERT="${LOCAL_CA_CERT:-${HOME}/certs/local_ca.crt}"

echo "[$(date -Iseconds)] Waiting for ${CA_URL} to be reachable..."
while true; do
    if [ ! -f "${CA_CERT}" ]; then
        echo "[$(date -Iseconds)] Waiting for step-ca to initialize..."
        sleep 10
        continue
    fi
    STATUS="$(step ca health --ca-url "${CA_URL}" --root "${CA_CERT}" 2>&1)"
    if [ "${STATUS}" = "ok" ]; then
        break
    else
        echo "[$(date -Iseconds)] ${STATUS}"
        sleep 10
    fi
done

mkdir -p ${CERTS_OUT}
# Generate a helper script that can be used interactively to issue certs
ISSUE_CERT="${HOME}/issue-cert"

if [ ! -f "${ISSUE_CERT}" ]; then
    # Pass variables from entrypoint by allowing variable substitution
    cat << EOF > "${ISSUE_CERT}" 
#!/bin/sh
set -e
ISSUER="${ISSUER}"
TOKEN_PASS="${TOKEN_PASS}"
CA_URL="${CA_URL}"
CERTS_OUT="${CERTS_OUT}"
EOF

    # Write the rest of the script without substitution
    cat << 'EOF' >> "${ISSUE_CERT}" 
function usage() {
    echo "Usage: $0 FQDN"
    exit 1
}
echo $@
FQDN="${1}"
if [ "${FQDN}" = "" ] || [ "${FQDN}" = "-h" ] || [ "${FQDN}" = "--help" ]; then
    usage
fi

echo "Issuing cert for ${FQDN}"
CERT="${CERTS_OUT}/${FQDN}.crt"
KEY="${CERTS_OUT}/${FQDN}.key"
TOKEN=$(step ca token "${FQDN}" --issuer "${ISSUER}" --password-file "${PROVISIONER_PASSWORD_FILE}" --ca-url "${CA_URL}" )
step ca certificate --force --token "${TOKEN}" --ca-url "${CA_URL}" "${FQDN}" "${CERT}" "${KEY}"
EOF

    chmod 0500 "${ISSUE_CERT}"
fi

if [ "${ISSUE_FQDN}" = "" ]; then
    exec /bin/sh
else
    exec "${HOME}/issue-cert" "${ISSUE_FQDN}"
fi
