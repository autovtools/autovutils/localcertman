#!/bin/bash
set -e
function usage() {
    echo "Usage: $0 FQDN [IP]"
    exit 1
}
FQDN="${1}"
if [ "$#" -lt 1 ] || [ "${FQDN}" = "-h" ] || [ "${FQDN}" = "--help" ]; then
    usage
fi

IP="${2}"
#docker-compose down --volumes && docker-compose build
docker-compose run --rm step-cli "${FQDN}"

# Copy from the ca-container, which is still running
CERTS="/home/step/certs/"
BUNDLE="localcertman_bundle.crt"
ROOT="localcertman_root.crt"
for filename in "${BUNDLE}" "${ROOT}" ${FQDN}.crt ${FQDN}.key; do
    docker cp "localcertman_step-ca_1:${CERTS}${filename}" ./certs
    if [ -x "$(command -v "chcon")" ]; then
        # If we are on a SELinux system, change the SELinux context
        #   to allow containers to mount this file
        chcon -t svirt_sandbox_file_t "./certs/${filename}" 2>/dev/null || true
    fi
done

# Bundle the root cert with the generated cert (complete the chain)
#   Some clients require this
cat "./certs/${ROOT}" >> "./certs/${FQDN}.crt"
ls -alh ./certs/${FQDN}.*

if [ "${IP}" != "" ]; then
    sudo sed -i "/${FQDN}/d" /etc/hosts || true
    echo "${IP} ${FQDN}" | sudo tee -a /etc/hosts
fi

cat << EOF
# To run a test server:
./test-server.sh ${FQDN}

# To test connection:
# NOTE: client will 'hang' until you type a response on the test server
#   (e.g. HELLO <CTRL+D>)
curl https://${FQDN}:4433
firefox https://${FQDN}:4433

EOF
