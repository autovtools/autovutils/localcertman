#!/bin/sh
set -e
function usage(){
    echo "Usage: $0 FQDN [PORT]"
    echo "If not given, PORT defaults to 4433"
    exit 1
}

if [ "$#" -lt 1 ]; then
    usage
fi
FQDN="${1}"
CERTS="./certs"
CERT="${CERTS}/${FQDN}.crt"
KEY="${CERTS}/${FQDN}.key"
PORT="${2:-4433}"

echo "Listening on ${PORT}"
openssl s_server \
    -cert "${CERT}" \
    -key "${KEY}" \
    -port "${PORT}"
