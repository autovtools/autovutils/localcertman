#!/bin/sh
set -e
# Use the CA bundle that contains intermediates
CA_BUNDLE="./certs/localcertman_bundle.crt"
CA_ROOT="./certs/localcertman_root.crt"

if [ ! -f "${CA_BUNDLE}" ]; then
    echo "CA_BUNDLE ${CA_BUNDLE} not found!"
    exit 1
fi
if [ ! -f "${CA_ROOT}" ]; then
    echo "CA_ROOT ${CA_ROOT} not found!"
    exit 1
fi
if [ -x "$(command -v update-ca-certificates)" ]; then
    # Debian / Ubuntu
    DEST="/usr/local/share/ca-certificates/"
    sudo cp "${CA_BUNDLE}" "${DEST}"
    sudo update-ca-certificates
elif [ -x "$(command -v update-ca-trust)" ]; then
    # RedHat
    DEST="/etc/pki/ca-trust/source/anchors/"
    sudo cp "${CA_BUNDLE}" "${DEST}"
    sudo update-ca-trust
else
    echo "Unsure how to install root cert"
    echo "Manually install: ${CA_BUNDLE}"
    exit 1
fi

echo "Installed ${CA_BUNDLE} at ${DEST}"

# Try to install the cert for firefox
# https://support.mozilla.org/en-US/kb/setting-certificate-authorities-firefox
CERTS64="/usr/lib64/mozilla/certificates"
CERTS="/usr/lib/mozilla/certificates"
CERTS_PATH=
POLICIES_SUFFIX="firefox/distribution/policies.json"
POLICIES64="/usr/lib64/${POLICIES_SUFFIX}"
POLICIES_PATH=
POLICIES="/usr/lib/${POLICIES_SUFFIX}"

if [ -d "${CERTS64}" ]; then
    CERTS_PATH=${CERTS64}
    POLICIES_PATH=${POLICIES64}
elif [ -d "${CERTS}" ]; then
    CERTS_PATH=${CERTS}
    POLICIES_PATH=${POLICIES}
else
    echo "Firefox not installed"
    exit 0
fi

sudo cp "${CA_ROOT}" "${CERTS_PATH}"
sudo chmod 644 "${CERTS_PATH}/$(basename "${CA_ROOT}")"
echo "Installed ${CA_BUNDLE} at ${CERTS_PATH}"
if [ ! -f "${POLICIES_PATH}" ]; then
    sudo cp policies.json "${POLICIES_PATH}"
    sudo chmod 644 "${POLICIES_PATH}"
    echo "Installed policies.json at ${POLICIES_PATH}"
else
    echo "${POLICIES_PATH}" already exists
fi

echo "All done!"
